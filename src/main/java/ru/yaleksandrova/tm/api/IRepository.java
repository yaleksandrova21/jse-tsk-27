package ru.yaleksandrova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

     void add(@NotNull final E entity);

     void addAll(@NotNull List<E> entities);

     void remove(@NotNull final E entity);

     @NotNull
     List<E> findAll();

     @NotNull
     List<E> findAll(@NotNull Comparator<E> comparator);

     void clear();

     int size();

     @NotNull
     boolean existsById(@NotNull final String id);

     @NotNull
     E findById(@NotNull final String id);

     @NotNull
     E findByIndex(@NotNull final Integer index);

     @NotNull
     E removeById(@NotNull String id);

     @NotNull
     E removeByIndex(@NotNull String userId, Integer index);

}
