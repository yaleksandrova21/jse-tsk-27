package ru.yaleksandrova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.dto.Domain;
import ru.yaleksandrova.tm.exception.entity.EntityNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new EntityNotFoundException();
        serviceLocator.getUserService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().addAll((domain.getUsers()));
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getAuthService().logout();
    }

}
