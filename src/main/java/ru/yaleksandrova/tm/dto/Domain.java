package ru.yaleksandrova.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}
